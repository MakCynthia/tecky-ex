const peter = {
  name: 'Peter',
  age: 50,
  students: [
    { name: 'Andy', age: 20 },
    { name: 'Bob', age: 23 },
    {
      name: 'Charlie',
      age: 25,
      exercises: [{ score: 60, date: new Date('2019-01-05') }],
    },
  ],
}

console.log('Peter:', peter)

function findStudent(name) {
  let studentFound
  for (let student of peter.students) {
    if (student.name == name) {
      studentFound = student
    }


  }
  if (studentFound) {
    console.log('student found:', studentFound)
  } else {
    console.log(name + ' is not found.')
  }
}

findStudent('Andy')
findStudent('Peter')
findStudent('May')


