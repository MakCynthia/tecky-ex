const peter = {
    name: "Peter",
    age: 50,
    students: [
        { name: "Andy", age: 20 },
        { name: "Bob", age: 23 },
        {
            name: "Charlie", age: 25, exercises: [
                { score: 60, date: new Date("2019-01-05") }
            ]
        }
    ]
}

console.log("peter:" + peter)

for (let student of peter.students) {
    console.log(`Student ${student.name} is ${student.age} years old`)
}


let studentFound;
for (let student of peter.students) {
    if (student.name === "Andy") {
        studentFound = student;
    }
}

if (studentFound){
    console.log("student found: " + studentFound) //??
}else {
    console.log("student not found")
}